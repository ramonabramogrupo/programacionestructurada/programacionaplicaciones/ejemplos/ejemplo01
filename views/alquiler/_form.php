<?php

use app\models\Alquiler;
use app\models\Clientes;
use app\models\Coches;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/** @var View $this */
/** @var Alquiler $model */
/** @var ActiveForm $form */
?>

<div class="alquiler-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        // opcion 1
        // realizar todas las tareas en la vista
        // select * from clientes
        //$clientes= app\models\Clientes::find()->all();
        // convierte el resultado de la consulta
        // en un array para el dropdownlist
        //$datos= yii\helpers\ArrayHelper::map($clientes, "nif", "nombre");
        
        //echo $form->field($model, 'cliente')->dropDownList($datos);
    
        // opcion 2
        // el modelo se encarga de realizar el tratamiento de datos
        echo $form->field($model, 'cliente')->dropDownList(Clientes::dropDown());
    
    ?>

    <?php 
        // opcion 1
        //$coches= Coches::find()->all();
        //$datos= ArrayHelper::map($coches, "bastidor", "modelo");
        //echo $form->field($model, 'coche')->dropDownList($datos); 
    
        // opcion 2
        echo $form->field($model, 'coche')->dropDownList(Coches::dropDown()); 
    
     ?>

    <?= $form->field($model, 'fechaAlquiler')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
